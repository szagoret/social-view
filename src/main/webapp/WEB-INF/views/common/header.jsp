<%@ page contentType="text/html; charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib uri="http://jawr.net/tags" prefix="jwr"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="titleKey">
	<tiles:getAsString name="titleKey" />
</c:set>
<title><spring:message code="${titleKey}"></spring:message></title>
<link rel="shortcut icon" href="resources/img/logo/social_network.png">
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<jwr:style src="/common.css" />