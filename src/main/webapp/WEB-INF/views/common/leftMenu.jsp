<%@ page contentType="text/html; charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="col-sm-2 no-padding-left">
	<!-- left -->
	<h4>
		<i class="glyphicon glyphicon-briefcase"></i>
		<spring:message code="home.toolbox" />
	</h4>
	<hr>

	<ul class="nav nav-stacked">
		<li><a href="javascript:;"><i class="ion-ios-speedometer"></i>
				<spring:message code="leftMenu.dashboard" /></a></li>
		<li><a href="javascript:;"><i class="fa fa-link"></i> <spring:message
					code="leftMenu.relationship" /></a></li>
		<li><a href="javascript:;"><i class="fa fa-group"></i> <spring:message
					code="leftMenu.authors" /></a></li>
		<li><a href="javascript:;"><i class="fa fa-book"></i> <spring:message
					code="leftMenu.journals" /></a></li>
		<li><a href="javascript:;"><i class="fa  fa-newspaper-o"></i>
				<spring:message code="leftMenu.publications" /></a></li>
	</ul>

	<hr>

</div>
<!-- /span-2 -->