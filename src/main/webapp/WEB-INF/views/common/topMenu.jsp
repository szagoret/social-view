<%@ page contentType="text/html; charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!-- Header -->
<div id="top-nav" class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="icon-toggle"></span>
			</button>
			<a class="navbar-brand" href="javascript:void(0)"> <img
				src="resources/img/logo/social_network.png" width="24" height="24" />
				<spring:message code="app.name" /></a>
		</div>
		<div class="navbar-collapse collapse">

			<!-- search form -->
			<div class="col-sm-7 col-md-offset-1">
				<div class="input-group input-group-sm">
					<input type="text" class="form-control" placeholder="Search for...">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div>
				<!-- /input-group -->
			</div>
			<!-- /.col-sm-7 -->
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown"><a class="dropdown-toggle" role="button"
					data-toggle="dropdown" href="#"> <i
						class="glyphicon glyphicon-user"></i> Sergiu <span class="caret"></span></a>
					<ul id="g-account-menu" class="dropdown-menu" role="menu">
						<li><a href="#"><i class="fa fa-user"></i> <spring:message
									code="header.profilDetails" /></a></li>
						<li><a href="?locale=en"><i class="fa fa-language"></i>&nbsp;EN</a></li>
						<li><a href="?locale=fr"><i class="fa fa-language"></i>&nbsp;FR</a></li>
						<li><a href="?locale=ro"><i class="fa fa-language"></i>&nbsp;RO</a></li>
						<li><a href="?locale=ru"><i class="fa fa-language"></i>&nbsp;RU</a></li>
					</ul></li>
			</ul>
		</div>
	</div>
	<!-- /container -->
</div>
<!-- /Header -->