<%@ page contentType="text/html; charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="col-sm-10">

	<!-- Small boxes (Stat box) -->
	<div class="row">
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3>30000</h3>
					<p>
						<spring:message code="leftMenu.publications" />
					</p>
				</div>
				<div class="icon">
					<small><i class="fa  fa-newspaper-o"></i></small>
				</div>
				<a href="#" class="small-box-footer"><spring:message
						code="home.moreInfo" /> <i class="fa fa-arrow-circle-right"></i> </a>
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-green">
				<div class="inner">
					<h3>53</h3>
					<p>
						<spring:message code="leftMenu.journals" />
					</p>
				</div>
				<div class="icon">
					<small><i class="fa fa-book"></i></small>
				</div>
				<a href="#" class="small-box-footer"><spring:message
						code="home.moreInfo" /> <i class="fa fa-arrow-circle-right"></i>
				</a>
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3>44</h3>
					<p>
						<spring:message code="leftMenu.authors" />
					</p>
				</div>
				<div class="icon">
					<small><i class="ion ion-person-add"></i></small>
				</div>
				<a href="#" class="small-box-footer"> <spring:message
						code="home.moreInfo" /> <i class="fa fa-arrow-circle-right"></i>
				</a>
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-red">
				<div class="inner">
					<h3>65</h3>
					<p>
						<spring:message code="home.scientificAreas" />
					</p>
				</div>
				<div class="icon">
					<small><i class="ion ion-ios7-pricetag-outline"></i></small>
				</div>
				<a href="#" class="small-box-footer"><spring:message
						code="home.moreInfo" /> <i class="fa fa-arrow-circle-right"></i>
				</a>
			</div>
		</div>
		<!-- ./col -->
	</div>
	<!-- /.row -->

	<div class="row">
		<!-- center left-->
		<div class="col-md-12">
			<div class="well">
				Inbox Messages <span class="badge pull-right">3</span>
			</div>

			<hr>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>Processing Status</h4>
				</div>
				<div class="panel-body">

					<small>Complete</small>
					<div class="progress">
						<div class="progress-bar progress-bar-success" role="progressbar"
							aria-valuenow="72" aria-valuemin="0" aria-valuemax="100"
							style="width: 72%">
							<span class="sr-only">72% Complete</span>
						</div>
					</div>
					<small>In Progress</small>
					<div class="progress">
						<div class="progress-bar progress-bar-info" role="progressbar"
							aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
							style="width: 20%">
							<span class="sr-only">20% Complete</span>
						</div>
					</div>
					<small>At Risk</small>
					<div class="progress">
						<div class="progress-bar progress-bar-danger" role="progressbar"
							aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
							style="width: 80%">
							<span class="sr-only">80% Complete</span>
						</div>
					</div>
				</div>
				<!--/panel-body-->
			</div>
			<!--/panel-->
		</div>
	</div>
</div>
<!--/col-->