package com.socialview;

import java.io.UnsupportedEncodingException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import com.socialview.entity.Author;

public class Main {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws UnsupportedEncodingException {

		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
				"/spring/applicationConfig.xml");

		LocalSessionFactoryBean sessionFactory = applicationContext
				.getBean(LocalSessionFactoryBean.class);
		SessionFactoryImpl sessionFactoryImpl = (SessionFactoryImpl) sessionFactory
				.getObject();
		Session session = sessionFactoryImpl.openSession();
		// Session session = sessionFactoryImpl.getCurrentSession();
		session.beginTransaction();
		Author igor = (Author) session.get(Author.class, 266L);
		// session.getEntityName(Author.class);
		/*
		 * EntityManagerFactory managerFactory = Persistence
		 * .createEntityManagerFactory("punit");
		 * 
		 * EntityManager entityManager = managerFactory.createEntityManager();
		 * 
		 * EntityTransaction tx = entityManager.getTransaction();
		 */
		// tx.begin();

		// Author author = entityManager.find(Author.class, 266L);
		// author.getFirstName();
		// author.getAuthorDetails();
		// author.getPublications();
		//
		// Journal journal = entityManager.find(Journal.class, 205L);
		// List<JournalCategory> journalCategories = journal
		// .getJournalCategories();
		// for (JournalCategory journalCategory : journalCategories) {
		// journalCategory.getCategory();
		// }
		// Set<Publication> publications = author.getPublications();
		// for (Publication publication : publications) {
		// System.out.println(new String(publication.toString().getBytes(
		// "UTF-8"), Charset.forName("UTF-8")));
		//
		// }
		//
		// // dynamic query
		// Query query = entityManager.createQuery("SELECT c FROM Author c");
		// List<Author> persons = query.getResultList();
		// System.out.println("Result authors: " + persons.size());

		System.exit(0);
	}

}
