package com.socialview.web.controller;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

	@RequestMapping(value = { "/home", "/" })
	public ModelAndView showHome() {

		ModelAndView modelAndView = new ModelAndView("home");
		return modelAndView;
	}

	@PostConstruct
	public void ok() {
		System.out.println("post contrstut");
	}
}
