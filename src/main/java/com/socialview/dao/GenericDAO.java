package com.socialview.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDAO<Entity, ID extends Serializable> {
	public int countAll() throws Exception;

	public Entity save(Entity entity);

	public List<Entity> findAll();

	public Entity findById(Long id);
}
