package com.socialview.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.socialview.dao.GenericDAO;
import com.socialview.entity.BaseEntity;

public abstract class HibernateDAOImpl<Entity extends BaseEntity, ID extends Serializable>
		implements GenericDAO<Entity, ID> {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(HibernateDAOImpl.class);

	protected Class<Entity> entityClass;

	protected static final int DEFAULT_BATCH_SIZE = 20;

	@Autowired
	protected SessionFactory sessionFactory;

	protected Session currentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected HibernateDAOImpl(Class<Entity> entityClass) {
		this.entityClass = entityClass;
	}

	public int countAll() throws Exception {
		Long count = (Long) currentSession().createQuery(
				"SELECT COUNT(*) FROM " + entityClass.getName()).uniqueResult();
		LOGGER.info("Count of all " + entityClass.getName() + " :"
				+ count.intValue());
		return count.intValue();
	}

	@Override
	public Entity save(Entity entity) {
		LOGGER.info("Save entity " + entityClass.getName());
		Serializable id = currentSession().save(entity);
		entity.setId((Long) id);
		return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Entity> findAll() {
		LOGGER.info("Find all " + entityClass.getName());
		return currentSession().createCriteria(entityClass).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Entity findById(Long id) {
		LOGGER.info("Find by id " + entityClass.getName());
		return (Entity) currentSession().get(entityClass, id);
	}

}
