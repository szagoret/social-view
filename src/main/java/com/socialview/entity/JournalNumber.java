package com.socialview.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ibn_journal_numbers")
public class JournalNumber extends BaseEntity {

	@Column(name = "current_number")
	private String currentNumber;

	@Column(name = "format")
	private String format;

	@Temporal(TemporalType.DATE)
	@Column(name = "published_on")
	private Date publishedOn;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "registered_on")
	private Date registeredOn;

	@Column(name = "total_number")
	private Short totalNumber;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "journalNumber")
	private List<Publication> publications;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "journal_id")
	private Journal journal;

	public String getCurrentNumber() {
		return currentNumber;
	}

	public void setCurrentNumber(String currentNumber) {
		this.currentNumber = currentNumber;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Date getPublishedOn() {
		return (Date) publishedOn.clone();
	}

	public void setPublishedOn(Date publishedOn) {
		this.publishedOn = publishedOn;
	}

	public Date getRegisteredOn() {
		return (Date) registeredOn.clone();
	}

	public void setRegisteredOn(Date registeredOn) {
		this.registeredOn = registeredOn;
	}

	public Short getTotalNumber() {
		return totalNumber;
	}

	public void setTotalNumber(Short totalNumber) {
		this.totalNumber = totalNumber;
	}

	public List<Publication> getPublications() {
		return publications;
	}

	public void setPublications(List<Publication> publications) {
		this.publications = publications;
	}

	public Journal getJournal() {
		return journal;
	}

	public void setJournal(Journal journal) {
		this.journal = journal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((currentNumber == null) ? 0 : currentNumber.hashCode());
		result = prime * result + ((format == null) ? 0 : format.hashCode());
		result = prime * result + ((journal == null) ? 0 : journal.hashCode());
		result = prime * result
				+ ((publications == null) ? 0 : publications.hashCode());
		result = prime * result
				+ ((publishedOn == null) ? 0 : publishedOn.hashCode());
		result = prime * result
				+ ((registeredOn == null) ? 0 : registeredOn.hashCode());
		result = prime * result
				+ ((totalNumber == null) ? 0 : totalNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JournalNumber other = (JournalNumber) obj;
		if (currentNumber == null) {
			if (other.currentNumber != null)
				return false;
		} else if (!currentNumber.equals(other.currentNumber))
			return false;
		if (format == null) {
			if (other.format != null)
				return false;
		} else if (!format.equals(other.format))
			return false;
		if (journal == null) {
			if (other.journal != null)
				return false;
		} else if (!journal.equals(other.journal))
			return false;
		if (publications == null) {
			if (other.publications != null)
				return false;
		} else if (!publications.equals(other.publications))
			return false;
		if (publishedOn == null) {
			if (other.publishedOn != null)
				return false;
		} else if (!publishedOn.equals(other.publishedOn))
			return false;
		if (registeredOn == null) {
			if (other.registeredOn != null)
				return false;
		} else if (!registeredOn.equals(other.registeredOn))
			return false;
		if (totalNumber == null) {
			if (other.totalNumber != null)
				return false;
		} else if (!totalNumber.equals(other.totalNumber))
			return false;
		return true;
	}

}
