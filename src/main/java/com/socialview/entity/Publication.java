package com.socialview.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ibn_result_publications")
public class Publication extends BaseEntity {

	@Column(name = "title")
	private String title;

	@Column(name = "subtitle")
	private String subTitle;

	@Column(name = "registered_on")
	private Date registeredOn;

	@Column(name = "start_page")
	private Short startPage;

	@Column(name = "end_page")
	private Short endPage;

	@ManyToOne
	@JoinColumn(name = "country_id")
	private Country country;

	@ManyToMany(mappedBy = "publications")
	private Set<Author> authors;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "journal_number_id")
	private JournalNumber journalNumber;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public Date getRegisteredOn() {
		return (Date) registeredOn.clone();
	}

	public void setRegisteredOn(Date registeredOn) {
		this.registeredOn = registeredOn;
	}

	public Short getStartPage() {
		return startPage;
	}

	public void setStartPage(Short startPage) {
		this.startPage = startPage;
	}

	public Short getEndPage() {
		return endPage;
	}

	public void setEndPage(Short endPage) {
		this.endPage = endPage;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Set<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	public JournalNumber getJournalNumber() {
		return journalNumber;
	}

	public void setJournalNumber(JournalNumber journalNumber) {
		this.journalNumber = journalNumber;
	}

	@Override
	public String toString() {
		return "Publication [title=" + title + ", subTitle=" + subTitle
				+ ", registeredOn=" + registeredOn + ", startPage=" + startPage
				+ ", endPage=" + endPage + ", country=" + country
				+ ", authors=" + authors + ", journalNumber=" + journalNumber
				+ ", id=" + id + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((authors == null) ? 0 : authors.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((endPage == null) ? 0 : endPage.hashCode());
		result = prime * result
				+ ((journalNumber == null) ? 0 : journalNumber.hashCode());
		result = prime * result
				+ ((registeredOn == null) ? 0 : registeredOn.hashCode());
		result = prime * result
				+ ((startPage == null) ? 0 : startPage.hashCode());
		result = prime * result
				+ ((subTitle == null) ? 0 : subTitle.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Publication other = (Publication) obj;
		if (authors == null) {
			if (other.authors != null)
				return false;
		} else if (!authors.equals(other.authors))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (endPage == null) {
			if (other.endPage != null)
				return false;
		} else if (!endPage.equals(other.endPage))
			return false;
		if (journalNumber == null) {
			if (other.journalNumber != null)
				return false;
		} else if (!journalNumber.equals(other.journalNumber))
			return false;
		if (registeredOn == null) {
			if (other.registeredOn != null)
				return false;
		} else if (!registeredOn.equals(other.registeredOn))
			return false;
		if (startPage == null) {
			if (other.startPage != null)
				return false;
		} else if (!startPage.equals(other.startPage))
			return false;
		if (subTitle == null) {
			if (other.subTitle != null)
				return false;
		} else if (!subTitle.equals(other.subTitle))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}
