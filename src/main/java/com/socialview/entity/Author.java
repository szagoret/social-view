package com.socialview.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Cacheable(true)
@Table(name = "authors")
@AttributeOverride(name = "id", column = @Column(name = "uid"))
public class Author extends BaseEntity {

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "father_name")
    private String fatherName;

    @Temporal(TemporalType.DATE)
    @Column(name = "birth_date")
    private Date birthDate;

    @Embedded
    private AuthorDetails authorDetails;

    // colectia este Set, deoarece exista o tabela intermediara
    // si daca vom folosi List, Hibernate la stergere mai intii
    // pe toate din lista apoi le va adauga inapoi cele care nu au fost sterse,
    // In caz ca este Set, se stie ca sunt unice, deaceea se sterge si atit
    @ManyToMany
    @JoinTable(name = "ibn_person_result_publications",
            joinColumns = @JoinColumn(name = "person_id",
                    referencedColumnName = "uid"),
            inverseJoinColumns = @JoinColumn(name = "result_publication_id",
                    referencedColumnName = "id"))
    private Set<Publication> publications;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public AuthorDetails getAuthorDetails() {
        return authorDetails;
    }

    public void setAuthorDetails(AuthorDetails authorDetails) {
        this.authorDetails = authorDetails;
    }

    public Set<Publication> getPublications() {
        return publications;
    }

    public void setPublications(Set<Publication> publications) {
        this.publications = publications;
    }

    public Date getBirthDate() {
        return (Date) birthDate.clone();
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Author [firstName=" + firstName + ", lastName=" + lastName
                + ", fatherName=" + fatherName + ", birthDate=" + birthDate
                + ", authorDetails=" + authorDetails + ", publications="
                + publications + ", id=" + id + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((authorDetails == null) ? 0 : authorDetails.hashCode());
        result = prime * result
                + ((fatherName == null) ? 0 : fatherName.hashCode());
        result = prime * result
                + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result
                + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result
                + ((publications == null) ? 0 : publications.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Author other = (Author) obj;
        if (authorDetails == null) {
            if (other.authorDetails != null) {
                return false;
            }
        } else if (!authorDetails.equals(other.authorDetails)) {
            return false;
        }
        if (fatherName == null) {
            if (other.fatherName != null) {
                return false;
            }
        } else if (!fatherName.equals(other.fatherName)) {
            return false;
        }
        if (firstName == null) {
            if (other.firstName != null) {
                return false;
            }
        } else if (!firstName.equals(other.firstName)) {
            return false;
        }
        if (lastName == null) {
            if (other.lastName != null) {
                return false;
            }
        } else if (!lastName.equals(other.lastName)) {
            return false;
        }
        if (publications == null) {
            if (other.publications != null) {
                return false;
            }
        } else if (!publications.equals(other.publications)) {
            return false;
        }
        return true;
    }

}
