package com.socialview.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AuthorDetails {

	public AuthorDetails() {
	}

	/**
	 * @param streetHome
	 * @param cpas
	 * @param mobilNumber
	 * @param phoneNumber
	 * @param nbPasport
	 */
	public AuthorDetails(String streetHome, String cpas, String mobilNumber,
			String phoneNumber, String nbPasport) {
		super();
		this.streetHome = streetHome;
		this.cpas = cpas;
		this.mobilNumber = mobilNumber;
		this.phoneNumber = phoneNumber;
		this.nbPasport = nbPasport;
	}

	@Column(name = "street_home")
	private String streetHome;

	@Column(name = "cpas")
	private String cpas;

	@Column(name = "mobil_number")
	private String mobilNumber;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "nr_pasport")
	private String nbPasport;

	public String getStreetHome() {
		return streetHome;
	}

	public void setStreetHome(String streetHome) {
		this.streetHome = streetHome;
	}

	public String getCpas() {
		return cpas;
	}

	public void setCpas(String cpas) {
		this.cpas = cpas;
	}

	public String getMobilNumber() {
		return mobilNumber;
	}

	public void setMobilNumber(String mobilNumber) {
		this.mobilNumber = mobilNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getNbPasport() {
		return nbPasport;
	}

	public void setNbPasport(String nbPasport) {
		this.nbPasport = nbPasport;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpas == null) ? 0 : cpas.hashCode());
		result = prime * result
				+ ((mobilNumber == null) ? 0 : mobilNumber.hashCode());
		result = prime * result
				+ ((nbPasport == null) ? 0 : nbPasport.hashCode());
		result = prime * result
				+ ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result
				+ ((streetHome == null) ? 0 : streetHome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorDetails other = (AuthorDetails) obj;
		if (cpas == null) {
			if (other.cpas != null)
				return false;
		} else if (!cpas.equals(other.cpas))
			return false;
		if (mobilNumber == null) {
			if (other.mobilNumber != null)
				return false;
		} else if (!mobilNumber.equals(other.mobilNumber))
			return false;
		if (nbPasport == null) {
			if (other.nbPasport != null)
				return false;
		} else if (!nbPasport.equals(other.nbPasport))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (streetHome == null) {
			if (other.streetHome != null)
				return false;
		} else if (!streetHome.equals(other.streetHome))
			return false;
		return true;
	}

}
