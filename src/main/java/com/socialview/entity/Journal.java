package com.socialview.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ibn_journals")
public class Journal extends BaseEntity {

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "journal_id")
	private List<JournalCategory> journalCategories;

	@Column(name = "description", columnDefinition = "TEXT")
	private String description;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "journal")
	private List<JournalNumber> journalNumbers;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "country_id")
	private Country country;

	public List<JournalCategory> getJournalCategories() {
		return journalCategories;
	}

	public void setJournalCategories(List<JournalCategory> journalCategories) {
		this.journalCategories = journalCategories;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<JournalNumber> getJournalNumbers() {
		return journalNumbers;
	}

	public void setJournalNumbers(List<JournalNumber> journalNumbers) {
		this.journalNumbers = journalNumbers;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime
				* result
				+ ((journalCategories == null) ? 0 : journalCategories
						.hashCode());
		result = prime * result
				+ ((journalNumbers == null) ? 0 : journalNumbers.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Journal other = (Journal) obj;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (journalCategories == null) {
			if (other.journalCategories != null)
				return false;
		} else if (!journalCategories.equals(other.journalCategories))
			return false;
		if (journalNumbers == null) {
			if (other.journalNumbers != null)
				return false;
		} else if (!journalNumbers.equals(other.journalNumbers))
			return false;
		return true;
	}

}
